#include <iostream>
#include <string>
using namespace std;

string B2S( bool value )
{
    return ( value ) ? "T" : "F";
}

bool TruthTable4b( bool p, bool q, bool r )
{
    return ( p && !(r || q) );
}

bool Sandwich( bool b, bool l, bool t, bool g )
{
    return ( !(b || l) && t && !g );
}

string SandwichInfo( bool b, bool l, bool t, bool g )
{
    string info = "";
    if ( b ) { info += "bacon "; } //else { info += "bacon no "; }
//    info += "\t";
    if ( l ) { info += "lettuce "; } //else { info += "lettuce no "; }
//    info += "\t";
    if ( t ) { info += "tomato "; } //else { info += "tomato no "; }
//    info += "\t";
    if ( g ) { info += "grapes"; } //else { info += "grapes no "; }

    return info;
}

int main()
{
//    cout << "p # q # r # p && !(r || q)" << endl;
//    cout << "--------------------" << endl;
//
//    for ( int p = 1; p >= 0; p-- )
//    {
//        for ( int q = 1; q >=0; q-- )
//        {
//            for ( int r = 1; r >= 0; r-- )
//            {
//                cout << B2S( p )
//                    << " # "
//                    << B2S( q )
//                    << " # "
//                    << B2S( r )
//                    << " #     "
//                    << B2S( TruthTable4b( p, q, r ) ) << endl;
//            }
//        }
//    }


    cout << "b # l # t # g \t# !(b || l) && t && !g" << endl;
    cout << "--------------------------------------" << endl;

    for ( int b = 1; b >= 0; b-- )
    {
        for ( int l = 1; l >=0; l-- )
        {
            for ( int t = 1; t >= 0; t-- )
            {
                for ( int g = 1; g >= 0; g-- )
                {
                    cout << B2S( b )
                        << " # "
                        << B2S( l )
                        << " # "
                        << B2S( t )
                        << " # "
                        << B2S( g )
                        << " \t# "
                        << B2S( Sandwich( b, l, t, g ) )
                        << "\t\t"
                        << SandwichInfo( b, l, t, g )
                        << endl;
                }
            }
        }
    }
}



