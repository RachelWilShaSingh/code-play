#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <ctime>
using namespace std;

struct Pixel
{
    int r;
    int g;
    int b;
};

struct PpmImage
{
    int width;
    int height;
    int colorDepth;
    vector<Pixel> pixels;
};

struct PpmImage2D
{
    // I don't care :|
    int width;
    int height;
    int colorDepth;
    Pixel pixelArray[32][32];

    PpmImage2D()
    {
        width = 32;
        height = 32;
    }
};

PpmImage LoadImage( const string& filename )
{
    ifstream input( filename );
    string strBuffer;
    int intBuffer;

    PpmImage image;

    getline( input, strBuffer ); // P3
    getline( input, strBuffer ); // Gimp comment

    input >> image.width >> image.height >> image.colorDepth;

    Pixel tempPixel;
    while ( input >> tempPixel.r >> tempPixel.g >> tempPixel.b )
    {
        image.pixels.push_back( tempPixel );//    PpmImage image;
//    image = LoadImage( "cat.ppm" );
//    ReversePixels( image );
//    SaveImage( image, "cat-edit.ppm" );

    }

    cout << "Image \"" << filename << "\" loaded." << endl
        << "\t Dimensions: " << image.width << "x" << image.height << endl
        << "\t " << image.pixels.size() << " total pixels" << endl;

    return image;
}

void SaveImage( const PpmImage& image, const std::string& filename )
{
    cout << "Saving PpmImage to " << filename << "..." << endl;
    ofstream output( filename );

    output << "P3" << endl << "# Blahblahblah" << endl
        << image.width << " "
        << image.height << endl
        << image.colorDepth << endl;

    for ( int i = 0; i < image.pixels.size(); i++ )
    {
        output
            << image.pixels[i].r << endl
            << image.pixels[i].g << endl
            << image.pixels[i].b << endl;
    }

    cout << "Tried to output the image file \"" << filename << "\". Hope it worked!" << endl;
}

void SaveImage( const PpmImage2D& image, const std::string& filename )
{
    cout << "Saving PpmImage2D to " << filename << "..." << endl;
    ofstream output( filename );

    output << "P3" << endl << "# Blahblahblah" << endl
        << image.width << " "
        << image.height << endl
        << image.colorDepth << endl;

    for ( int h = 0; h < image.height; h++ )
    {
        for ( int w = 0; w < image.width; w++ )
        {
            output
                << image.pixelArray[w][h].r << endl
                << image.pixelArray[w][h].g << endl
                << image.pixelArray[w][h].b << endl;
        }
    }

    cout << "Outputted image file \"" << filename << "\"." << endl;
}

void ReversePixels( PpmImage& image )
{
    vector<Pixel> reversed;

    for ( int i = image.pixels.size()-1; i >= 0; i-- )
    {
        reversed.push_back( image.pixels[i] );
    }

    image.pixels = reversed;
}

PpmImage2D MakeBrickWall()
{
    PpmImage2D image;
    image.colorDepth = 255;

    // Set the entire image to red
    for ( int y = 0; y < image.height; y++ )
    {
        for ( int x = 0; x < image.width; x++ )
        {
            image.pixelArray[x][y].r = 180;
            image.pixelArray[x][y].g = 0;
            image.pixelArray[x][y].b = 0;
        }
    }

    // Medium red
    int yPos = image.height / 2 - 1;
    for ( int x = 0; x < image.width; x++ )
    {
        image.pixelArray[x][yPos-1].r = 130;
        image.pixelArray[x][yPos-1].g = 0;
        image.pixelArray[x][yPos-1].b = 0;
    }

    yPos = image.height - 1;
    for ( int x = 0; x < image.width; x++ )
    {
        image.pixelArray[x][yPos-1].r = 130;
        image.pixelArray[x][yPos-1].g = 0;
        image.pixelArray[x][yPos-1].b = 0;
    }

    // Vertical lines
    int xPos = image.width / 2;
    yPos = image.height / 2;
    for ( int y = 0; y < yPos; y++ )
    {
        image.pixelArray[xPos-1][y].r = 130;
        image.pixelArray[xPos-1][y].g = 0;
        image.pixelArray[xPos-1][y].b = 0;
    }

    xPos = image.width - 1;
    for ( int y = yPos; y < image.height; y++ )
    {
        image.pixelArray[xPos-1][y].r = 130;
        image.pixelArray[xPos-1][y].g = 0;
        image.pixelArray[xPos-1][y].b = 0;
    }

    // Dark red

    // Horizontal line half way down
    yPos = image.height / 2 - 1;
    for ( int x = 0; x < image.width; x++ )
    {
        image.pixelArray[x][yPos].r = 50;
        image.pixelArray[x][yPos].g = 0;
        image.pixelArray[x][yPos].b = 0;
    }

    yPos = image.height - 1;
    for ( int x = 0; x < image.width; x++ )
    {
        image.pixelArray[x][yPos].r = 50;
        image.pixelArray[x][yPos].g = 0;
        image.pixelArray[x][yPos].b = 0;
    }

    // Vertical lines
    xPos = image.width / 2;
    yPos = image.height / 2;
    for ( int y = 0; y < yPos; y++ )
    {
        image.pixelArray[xPos][y].r = 50;
        image.pixelArray[xPos][y].g = 0;
        image.pixelArray[xPos][y].b = 0;
    }

    xPos = image.width - 1;
    for ( int y = yPos; y < image.height; y++ )
    {
        image.pixelArray[xPos][y].r = 50;
        image.pixelArray[xPos][y].g = 0;
        image.pixelArray[xPos][y].b = 0;
    }

    // Light red
    xPos = 1;
    yPos = image.height / 2 + 1;

    image.pixelArray[xPos][yPos].r = 220;
    image.pixelArray[xPos][yPos].g = 20;
    image.pixelArray[xPos][yPos].b = 20;


    image.pixelArray[xPos][yPos+1].r = 220;
    image.pixelArray[xPos][yPos+1].g = 20;
    image.pixelArray[xPos][yPos+1].b = 20;

    for ( int x = xPos; x < xPos + 5; x++ )
    {
        image.pixelArray[x][yPos].r = 220;
        image.pixelArray[x][yPos].g = 20;
        image.pixelArray[x][yPos].b = 20;
    }


    xPos = image.width / 2 + 2;
    yPos = 1;

    image.pixelArray[xPos][yPos].r = 220;
    image.pixelArray[xPos][yPos].g = 20;
    image.pixelArray[xPos][yPos].b = 20;

    image.pixelArray[xPos][yPos+1].r = 220;
    image.pixelArray[xPos][yPos+1].g = 20;
    image.pixelArray[xPos][yPos+1].b = 20;

    for ( int x = xPos; x < xPos + 5; x++ )
    {
        image.pixelArray[x][yPos].r = 220;
        image.pixelArray[x][yPos].g = 20;
        image.pixelArray[x][yPos].b = 20;
    }

    return image;
}

PpmImage2D MakeGrass()
{
    PpmImage2D image;
    image.colorDepth = 255;

    // background color
    for ( int y = 0; y < image.height; y++ )
    {
        for ( int x = 0; x < image.width; x++ )
        {
            image.pixelArray[x][y].r = 100;
            image.pixelArray[x][y].g = 180;
            image.pixelArray[x][y].b = 75;
        }
    }

    // Splotchies
    int splotchies = rand() % 5 + 20;

    for ( int i = 0; i < splotchies; i++ )
    {
        int x = rand() % image.width;
        int y = rand() % image.height;

        int width = rand() % 4 + 2;
        int height = rand() % 4 + 2;

        image.pixelArray[x][y].r = 82;
        image.pixelArray[x][y].g = 165;
        image.pixelArray[x][y].b = 56;

        for ( int w = 0; w < width; w++ )
        {
            for ( int h = 0; h < height; h++ )
            {
                image.pixelArray[x+w][y+h].r = 82;
                image.pixelArray[x+w][y+h].g = 165;
                image.pixelArray[x+w][y+h].b = 56;
            }
        }
    }

    // Grass blades
    int highlights = rand() % 50 + 50;

    for ( int i = 0; i < highlights; i++ )
    {
        int x = rand() % image.width;
        int y = rand() % image.height;

        image.pixelArray[x][y].r = 65;
        image.pixelArray[x][y].g = 150;
        image.pixelArray[x][y].b = 40;

        int addHighlight = rand() % 10;
        if ( addHighlight % 10 == 0 )
        {
            image.pixelArray[x][y-1].r = 135;
            image.pixelArray[x][y-1].g = 200;
            image.pixelArray[x][y-1].b = 115;
        }

        int height = rand() % 3;
        for ( int j = 0; j < height; j++ )
        {
            image.pixelArray[x][y+j].r = 65;
            image.pixelArray[x][y+j].g = 150;
            image.pixelArray[x][y+j].b = 40;
        }
    }

//    highlights = rand() % 50 + 50;
//
//    for ( int i = 0; i < highlights; i++ )
//    {
//        int x = rand() % image.width;
//        int y = rand() % image.height;
//
//        image.pixelArray[x][y].r = 65;
//        image.pixelArray[x][y].g = 150;
//        image.pixelArray[x][y].b = 40;
//    }

    return image;
}

PpmImage2D MakePerson()
{
    PpmImage2D image;
    image.colorDepth = 255;

    int headTop = 3;
    int headWidth = rand() % 6 + 24;
    int headHeight = image.height * 0.50 - headTop;

    // background color
    for ( int y = 0; y < image.height; y++ )
    {
        for ( int x = 0; x < image.width; x++ )
        {
            image.pixelArray[x][y].r = 255;
            image.pixelArray[x][y].g = 255;
            image.pixelArray[x][y].b = 255;
        }
    }

    Pixel skinColors[5];

    skinColors[0].r = 207;
    skinColors[0].g = 150;
    skinColors[0].b = 100;

    skinColors[1].r = 250;
    skinColors[1].g = 180;
    skinColors[1].b = 120;

    skinColors[2].r = 155;
    skinColors[2].g = 97;
    skinColors[2].b = 60;

    skinColors[3].r = 64;
    skinColors[3].g = 43;
    skinColors[3].b = 26;

    skinColors[4].r = 250;
    skinColors[4].g = 217;
    skinColors[4].b = 190;

    int skin = rand() % 5;

    int headX = image.width / 2 - headWidth / 2;

    // Draw head
    for ( int y = headTop; y < headHeight + headTop; y++ )
    {
        for ( int x = headX; x < headWidth + headX; x++ )
        {
            image.pixelArray[x][y].r = skinColors[skin].r;
            image.pixelArray[x][y].g = skinColors[skin].g;
            image.pixelArray[x][y].b = skinColors[skin].b;

            if ( y == headTop || y == headHeight + headTop - 1 || x == headX || x == headWidth + headX - 1 )
            {
                image.pixelArray[x][y].r = 0;
                image.pixelArray[x][y].g = 0;
                image.pixelArray[x][y].b = 0;
            }
        }
    }

    // Eyes
    int eyeSpacing = rand() % 5 + 5;

    int yPos = headTop + (headHeight / 2);

    int xPos = image.width / 2 - eyeSpacing;
    image.pixelArray[xPos][yPos].r = 0;
    image.pixelArray[xPos][yPos].g = 0;
    image.pixelArray[xPos][yPos].b = 0;

    image.pixelArray[xPos][yPos-1].r = 0;
    image.pixelArray[xPos][yPos-1].g = 0;
    image.pixelArray[xPos][yPos-1].b = 0;

    image.pixelArray[xPos][yPos+1].r = 0;
    image.pixelArray[xPos][yPos+1].g = 0;
    image.pixelArray[xPos][yPos+1].b = 0;

    xPos = image.width / 2 + eyeSpacing;
    image.pixelArray[xPos][yPos].r = 0;
    image.pixelArray[xPos][yPos].g = 0;
    image.pixelArray[xPos][yPos].b = 0;

    image.pixelArray[xPos][yPos-1].r = 0;
    image.pixelArray[xPos][yPos-1].g = 0;
    image.pixelArray[xPos][yPos-1].b = 0;

    image.pixelArray[xPos][yPos+1].r = 0;
    image.pixelArray[xPos][yPos+1].g = 0;
    image.pixelArray[xPos][yPos+1].b = 0;

    // Body
    int offset = (rand() % 10 + 2);
    int bodyTop = headTop + headHeight;
    int bodyBottom = image.height - 5;
    int bodyLeft = headX + offset;
    int bodyRight = headX + headWidth - offset;

    Pixel shirtColor;
    shirtColor.r = rand() % 255;
    shirtColor.g = rand() % 255;
    shirtColor.b = rand() % 255;

    for ( int y = bodyTop; y < bodyBottom; y++ )
    {
        for ( int x = bodyLeft; x < bodyRight; x++ )
        {
            image.pixelArray[x][y].r = shirtColor.r;
            image.pixelArray[x][y].g = shirtColor.g;
            image.pixelArray[x][y].b = shirtColor.b;

            if ( y == bodyTop || y == bodyBottom-1 || x == bodyLeft || x == bodyRight - 1 )
            {
                image.pixelArray[x][y].r = 0;
                image.pixelArray[x][y].g = 0;
                image.pixelArray[x][y].b = 0;
            }
        }
    }

    return image;
}

int main()
{
//    PpmImage image;
//    image = LoadImage( "cat.ppm" );
//    ReversePixels( image );
//    SaveImage( image, "cat-edit.ppm" );

    srand( time( NULL ) );

//    PpmImage2D brickWall = MakeBrickWall();
//    SaveImage( brickWall, "bricks.ppm" );
//
//    PpmImage2D grass = MakeGrass();
//    SaveImage( grass, "grass1.ppm" );
//
//    grass = MakeGrass();
//    SaveImage( grass, "grass2.ppm" );
//
//    grass = MakeGrass();
//    SaveImage( grass, "grass3.ppm" );

    PpmImage2D character = MakePerson();
    SaveImage( character, "character.ppm" );
}
